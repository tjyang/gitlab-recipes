You can find the NGINX configs in the [GitLab official repository][gitlab] which can
be used for the Linux package (Omnibus) or self-compiled installations.

You might need to make some adjustments depending on your installation method.

## Omnibus configs

[Omnibus packages][] use their own bundled nginx server. If you want to use your
own external Nginx server, follow the first 3 steps to
[configure GitLab][omnibusnginxext] and then download the appropriate config
file (ssl or non-ssl) from this directory.

After placing the configs in their appropriate location
(read [Different conf directories](#different-conf-directories)), make sure to
restart Nginx.

## CentOS related NGINX notes

### Different conf directories

If NGINX is installed through the package manager, adjust sites in `/etc/nginx/conf.d/`
instead of `/etc/nginx/sites-available/` or create those directories and tell `nginx`
to monitor them:

    sudo mkdir /etc/nginx/sites-{available,enabled}

Then edit `/etc/nginx/nginx.conf` and replace `include /etc/nginx/conf.d/*.conf;`
with `/etc/nginx/sites-enabled/*;`

### Give nginx access to git group

In order for GitLab to display properly you have to make either one of the changes
below. The first one is recommended.

- Add `nginx` user to `git` group:

  ```shell
  sudo usermod -a -G git nginx
  sudo chmod g+rx /home/git/
  ```

- Replace the default `nginx` user with `git` and group `root` in `/etc/nginx/nginx.conf`:

    #user             nginx;
    user              git root;

[gitlab]: https://gitlab.com/gitlab-org/gitlab/tree/master/lib/support/nginx "NGINX configs for GitLab"
[Omnibus packages]: https://about.gitlab.com/downloads/
[omnibusnginxext]: http://doc.gitlab.com/omnibus/settings/nginx.html#using-a-non-bundled-web-server
