## Centos - Sendmail

    su -
    yum -y install sendmail-cf
    cd /etc/mail
    vim /etc/mail/sendmail.mc

Add a line with the smtp gateway hostname

    define(`SMART_HOST', `smtp.example.com')dnl

Then replace this line:

    EXPOSED_USER(`root')dnl

with:

    dnl EXPOSED_USER(`root')dnl

Now enable these settings with:

    make
    chkconfig sendmail on

## Centos 6/7 - postfix 

Assuming we are on server01 to configure a postfix mail client in example.com subnet. The mail-relay server in example.com is remotesmtp.example.com. it also need an account/password authenication to enable your postfix client to send message out.


### Install mail related rpm packagesfor postfix
    yum install -y mailx postfix  net-tools # install postfix mailx client if not installed already.
### Check the current postfix mail status
    
    [root@server01 ~]# netstat -tuln |grep :25 
    tcp        0      0 127.0.0.1:25                0.0.0.0:*                   LISTEN      
    tcp        0      0 ::1:25                      :::*                        LISTEN      
    udp        0      0 fe80::250:56ff:fe8d:d1a8:123 :::*                                    
    [root@server01 ~]# 

### Configure postfix mail relay account 

    [root@server01 postfix]# cat /etc/postfix/relay_passwd
    remotesmtp.example.com mailaccount01:password
    [root@server01 postfix]# 

    chown root:root /etc/postfix/relay_passwd
    chmod 600 /etc/postfix/relay_passwd
    postmap /etc/postfix/relay_passwd # relay_passwd.db will be generated

### Bunch of bash commands to configure a postfix as mail client.

    nc -z remotesmtp.example.com 25 && netstat -tuln |grep :25 # to see if existing smtp port 25 is opened on remote relay mail host.
    echo "inet_interfaces = loopback-only" >> /etc/postfix/main.cf
    echo "myhostname = `hostname`.example.com" >> /etc/postfix/main.cf
    echo "mydomain = example.com" >> /etc/postfix/main.cf
    echo "myorigin = \$myhostname" >> /etc/postfix/main.cf
    echo "relayhost = smtp.example.com" >> /etc/postfix/main.cf 
    echo "smtp_sasl_auth_enable = yes" >> /etc/postfix/main.cf
    echo "smtp_sasl_password_maps = hash:/etc/postfix/relay_passwd " >> /etc/postfix/main.cf
    echo "smtp_sasl_tls_security_options = " >> /etc/postfix/main.cf
    echo "smtp_tls_security_level = may ">> /etc/postfix/main.cf
    echo "inet_protocols = ipv4" >> /etc/postfix/main.cf # centos/rhel 7.x only
    echo "root: logwatch@example.com" >> /etc/aliases && newaliases
    rm -f /var/spool/mail/* # to remove mails from leftover mails.
    postqueue -p # To see if there is mails queuded under postfix
    postsuper -d ALL # Delete them all
    service postfix restart && chkconfig postfix on # on centos 6
    systemctl restart postfix && systemctl enable postfix on # on centos 7
    nc -z remotesmtp.example.com 25 && netstat -tuln |grep :25  # make sure we can reach remote mail relay server
    # send out an email for send and receive test
    echo "This is a test" | mailx -r "logwatch@example.com" -s "`hostname`:Just a test" "logwatch@example.com"

## Forwarding all emails

Now we want all logging of the system to be forwarded to a central email address:

    su -
    echo adminlogs@example.com > /root/.forward
    chown root /root/.forward
    chmod 600 /root/.forward
    restorecon /root/.forward

    echo adminlogs@example.com > /home/git/.forward
    chown git /home/git/.forward
    chmod 600 /home/git/.forward
    restorecon /home/git/.forward


